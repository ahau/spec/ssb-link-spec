# ssb link spec

Links are a type of message which are designed to connect two other records or cipherlinks (similar to a SQL join-table entry).
By creating links as seperate records, it allows us to do things like allow anyone to make linkages, to comment on linkages,
and to store meta-data on links. 

## Format

If we're linking say a `story` record and a `artefact/photo` record, then the link message looks like:

```js
{
  type: 'link/story-artefact',
  parent: storyId,
  child: artefactId,

  tangles: {
    link: { root: null, previous: null }
  }
}
```

```mermaid
graph LR

story --> link([link/story/artefact]) --> artefact/photo

classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
```


NOTES:
- the link is _directed_ from parent -> child
- the `type` is always `link/${parentType}-${childType}`
    - we only take the "superType" of each record. i.e. `artefact`, not `artefact/photo`
- if you have multiple reasons that there might be a link between two superTypes, you can clarify with an additional subType
    - e.g. `link/story-profile/mention` and `link/story-profile/author`

### Updates

Once a link is created, the `parent` and `child` fields are never mutated.
    - This is so we can comment / attest to the validity of a link, and not have to worry about the endpoints changing and the attestation needs re-assessing
    - If you need to change the endpoints, do this by tombstoning this record and 

An example of an update to the above record:

```js
{
  type: 'link/story-artefact',

  tombstone: {
    set: {
      date: Date.now(),
      reason: 'got my photos mixed up!'
    }
  },

  tangles: {
    link: {
      root: '%AahKXVqPREXhRo380ZbESBgiWyimqfpzF63uU0Dhkac=.sha256',
      previous: ['%AahKXVqPREXhRo380ZbESBgiWyimqfpzF63uU0Dhkac=.sha256']
    }
  }
}
```

We know this is an update because it points to `tangles.link.root` which it is updating.
Also it contains no `parent`/ `child` field (remember we can't update these)

We use a convention of `field: { set: value }` as a way to indicate we're performing an
operation on a mutable field (as opposed to setting a static field)

### Meta Data

Example

```js
{
  type: 'link/profile-profile/child',
  parent: parentProfileId,
  child: childProfileId,

  whangai: { set: true },            // within-family adoption in Te Ao Maori
  legallyAdopted: { set: false },    // state registered adoption

  tangles: {
    link: { root: null, previous: null }
  }
}
```

### Encryption

#### 1. Endpoints have SAME recps

If the two enpoints of the link are records that have the same `recps` (encrypted to the same group/ people), then the `link` should inherit those same `recps`.


```mermaid
graph LR

subgraph private group
  profile
  story
  link([link/profile/story])
end
profile --> link --> story

classDef cluster fill:#1fdbde33, stroke:#1fdbde;
classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
classDef encrypted fill:white, stroke:purple, stroke-width 1, color: black, font-family:sans, font-size: 14px;
class profile,story,link encrypted;
```

#### 2. Endpoints have DIFFERENT recps

If the two endpoints of a link have **different** `recps`, then you need to carefully consider which recipients should know of the existence of a link, as being
able to decrypt the link reveals the existence of both ends.

##### 2A. One public, One encrypted

You should encrypt the link to the same as the encrypted end.

```mermaid
graph LR

profile
subgraph private group
  story
  link([link/profile/story])
end
profile --> link --> story

classDef cluster fill:#1fdbde33, stroke:#1fdbde;
classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
classDef encrypted fill:white, stroke:purple, stroke-width 1, color: black, font-family:sans, font-size: 14px;
class story,link encrypted;
```

In this diagram a public profile has been linked to an encrypted story.
This seems wise because we don't want to advertise the existence of a story about someone to just anyone.

##### 2B. Two differently encrypted 

This needs to be done carefully, as the link will reveal an author within another group
(assuming you use a raw messageId, you reveal the `author` of the root message of that record)

```mermaid
graph LR

subgraph group 1
  profile
  link([link/profile/story])
end
subgraph group 2
  story
end
profile --> link --> story

classDef cluster fill:#1fdbde33, stroke:#1fdbde;
classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
classDef encrypted fill:white, stroke:purple, stroke-width 1, color: black, font-family:sans, font-size: 14px;
class profile,link,story encrypted;
```

Same problem here:

```mermaid
graph LR

subgraph group 1
  profile
end
subgraph group 2
  link([link/profile/story])
  story
end
profile --> link --> story

classDef cluster fill:#1fdbde33, stroke:#1fdbde;
classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
classDef encrypted fill:white, stroke:purple, stroke-width 1, color: black, font-family:sans, font-size: 14px;
class profile,link,story encrypted;
```

The "safe" case is where one group is a "subgroup" of another, by which it's meant that all members of the "subgroup"
are also members of the parent group.

In this case you should encrypt the link to the subgroup:

```mermaid
graph LR

subgraph group
  profile

  subgraph subgroup
    link([link/profile/story])
    story
  end
end
profile --> link --> story

classDef cluster fill:#1fdbde33, stroke:#1fdbde;
classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
classDef encrypted fill:white, stroke:purple, stroke-width 1, color: black, font-family:sans, font-size: 14px;
class profile,link,story encrypted;
```

#### 3. Linking to a GroupId

GroupId's use the [cloaked message spec], so while they refer to an encrypted message (the `group/init) message,
the actual message being reference can only be known to those in the group. i.e. it's safe to share publicly.

This makes it safe to link this publicly too, e.g.

```mermaid
graph LR

groupId
link([link/group-profile])
profile
groupId --> link --> profile

classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
```

Or you can encrypt it so only group members know about the reference:
```mermaid
graph LR

groupId
subgraph group
  link([link/group-profile])
  profile
end
groupId --> link --> profile

classDef cluster fill:#1fdbde33, stroke:#1fdbde;
classDef default fill:#990098, stroke:purple, stroke-width 1, color: white, font-family:sans, font-size: 14px;
classDef encrypted fill:white, stroke:purple, stroke-width 1, color: black, font-family:sans, font-size: 14px;
class link,profile encrypted;
```

#### 4. :warning: Encrypting a link to two groups

You cannot currently encrypt a link to two groups.
This is forbidden by the current version of the [private-group-spec] because:
- it would leak group tangle info, which in turn leaks group membership info
- there's only on `tangles.group` slot on the message


[private-group-spec]: https://www.github.com/sbbc/private-group-spec
[cloaked message spec]: https://github.com/ssbc/envelope-spec/tree/master/cloaked_msg_id
